export const NotificationMessage = {
  LOADING: 'Loading',
  ERROR: 'Could not fetch data!',
  SUCCESS: 'Operation Successful!',
};
