export const userInfoKey = {
  NAME: 'name',
  USERNAME: 'username',
  EMAIL: 'email',
  PHONE: 'phone',
  WEBSITE: 'website',
  ADDRESS_CITY: 'addressCity',
  ADDRESS_STREET: 'addressStreet',
  ADDRESS_SUITE: 'addressSuite',
  ADDRESS_ZIPCODE: 'addressZipcode',
  COMPANY_BS: 'companyBS',
  COMPANY_CATCH_PHRASE: 'companyCatchPhrase',
  COMPANY_NAME: 'companyName',
};
