export const NotificationType = {
  LOADING: 'loading',
  ERROR: 'error',
  SUCCESS: 'success',
};
