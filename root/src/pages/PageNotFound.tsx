export default function PageNotFound() {
  return (
    <h3 style={{ textAlign: 'center', color: 'darkblue', marginTop: '0' }}>
      Page not found!
    </h3>
  );
}
