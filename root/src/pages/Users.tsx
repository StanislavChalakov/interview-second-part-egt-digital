import { useAppDispatch, useAppSelector } from '../../hooks';
import { useEffect, useRef, useState } from 'react';
import { fetchUsersData } from '../store/users-actions';
import { Alert, Avatar, Collapse, message } from 'antd';
import { NotificationType } from '../common/notification-type.enum';
import defaultAvatar from '../assets/defaultAvatar.jpg';
import UserInfo from '../components/UserInfo/UserInfo';
import { uiActions } from '../store/ui-slice';

const { Panel } = Collapse;

export default function Users() {
  const { users } = useAppSelector((state) => state.users);
  const dispatch = useAppDispatch();
  const isInitialFetch = useRef(true);
  const { showNotification, notificationType, notificationMessage, showToast } =
    useAppSelector((state) => state.ui);
  const [messageApi, contextHolder] = message.useMessage();
  const [userIdForEdit, setUserIdForEdit] = useState(0);
  const [collapseActiveKey, setCollapseActiveKey] = useState(0);

  useEffect(() => {
    if (isInitialFetch.current) {
      isInitialFetch.current = false;
      dispatch(fetchUsersData());
    }
  }, []);

  useEffect(() => {
    if (showToast) {
      if (notificationType === NotificationType.SUCCESS) {
        messageApi.open({
          type: 'success',
          content: notificationMessage,
        });
      } else if (notificationType === NotificationType.ERROR) {
        messageApi.open({
          type: 'error',
          content: notificationMessage,
        });
      }

      dispatch(uiActions.toggleToast(false));
    }
  }, [notificationType]);

  const changeCollapseKeyHandler = (key: string | string[]) => {
    if (key.length > 0) {
      setCollapseActiveKey(+key[0]);
    } else {
      setCollapseActiveKey(0);
    }
  };

  return (
    <>
      {contextHolder}
      {showNotification && notificationType === NotificationType.ERROR && (
        <Alert type="error" message={notificationMessage} />
      )}
      {users.length > 0 ? (
        <>
          <h3
            style={{ textAlign: 'center', color: 'darkblue', marginTop: '0' }}
          >
            Users found: {users.length}
          </h3>
          <Collapse accordion onChange={changeCollapseKeyHandler}>
            {users.map((user) => (
              <Panel
                header={
                  <>
                    <Avatar src={defaultAvatar} />
                    <span
                      style={{
                        marginLeft: '1rem',
                        fontWeight: 'bold',
                        color: 'darkblue',
                      }}
                    >
                      {user.name}
                    </span>
                  </>
                }
                key={user.id}
              >
                <UserInfo
                  user={user}
                  userIdForEdit={userIdForEdit}
                  setUserIdForEdit={setUserIdForEdit}
                  collapseActiveKey={collapseActiveKey}
                />
              </Panel>
            ))}
          </Collapse>
        </>
      ) : (
        notificationType !== NotificationType.LOADING && (
          <Alert type="info" message="No users found!" />
        )
      )}
    </>
  );
}
