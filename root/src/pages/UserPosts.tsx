import { useEffect, useRef, useState } from 'react';
import { useAppDispatch, useAppSelector } from '../../hooks';
import { fetchUserPostsData } from '../store/posts-actions';
import { useParams } from 'react-router-dom';
import UserInfo from '../components/UserInfo/UserInfo';
import { NotificationType } from '../common/notification-type.enum';
import { Alert, Card, message } from 'antd';
import UserPost from '../components/UserPost/UserPost';
import { uiActions } from '../store/ui-slice';

export default function Posts() {
  const { user } = useAppSelector((state) => state.userPosts);
  const { posts } = useAppSelector((state) => state.userPosts);
  const dispatch = useAppDispatch();
  const isInitialFetch = useRef(true);
  const { showNotification, notificationType, notificationMessage, showToast } =
    useAppSelector((state) => state.ui);
  const { userId } = useParams();
  const [messageApi, contextHolder] = message.useMessage();
  const [userIdForEdit, setUserIdForEdit] = useState(0);
  const [postIdForEdit, setPostIdForEdit] = useState(0);

  useEffect(() => {
    if (isInitialFetch.current) {
      isInitialFetch.current = false;
      dispatch(fetchUserPostsData(userId));
    }
  }, []);

  useEffect(() => {
    if (showToast) {
      if (notificationType === NotificationType.SUCCESS) {
        messageApi.open({
          type: 'success',
          content: notificationMessage,
        });
      } else if (notificationType === NotificationType.ERROR) {
        messageApi.open({
          type: 'error',
          content: notificationMessage,
        });
      }

      dispatch(uiActions.toggleToast(false));
    }
  }, [notificationType]);

  return (
    <>
      {contextHolder}
      {showNotification && notificationType === NotificationType.ERROR && (
        <Alert type="error" message={notificationMessage} />
      )}
      {user ? (
        <UserInfo
          user={user}
          userIdForEdit={userIdForEdit}
          setUserIdForEdit={setUserIdForEdit}
        />
      ) : (
        !notificationType && (
          <Alert type="info" message="No details found for selected user!" />
        )
      )}
      {posts.length > 0 ? (
        <Card
          title={`User Posts: ${posts.length}`}
          style={{ marginTop: '0.5rem' }}
        >
          {posts.map((post) => (
            <UserPost
              key={post.id}
              post={post}
              postIdForEdit={postIdForEdit}
              setPostIdForEdit={setPostIdForEdit}
            />
          ))}
        </Card>
      ) : (
        notificationType !== NotificationType.LOADING && (
          <Alert type="info" message="No posts found!" />
        )
      )}
    </>
  );
}
