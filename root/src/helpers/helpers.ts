export function containsLettersOnly(str: string) {
  return /^[a-zA-Z]+$/.test(str);
}

export function containsOnlyLettersAndDigits(str: string) {
  return /^[a-zA-Z0-9]+$/.test(str);
}

export function isValidEmail(str: string) {
  return /\S+@\S+\.\S+/.test(str);
}
