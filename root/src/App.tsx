import './App.css';
import { Route, Routes } from 'react-router-dom';
import Users from './pages/Users';
import UserPosts from './pages/UserPosts';
import PageNotFound from './pages/PageNotFound';

function App() {
  return (
    <Routes>
      <Route path="/" element={<Users />} />
      <Route path="/users/:userId/posts" element={<UserPosts />} />
      <Route path="/*" element={<PageNotFound />} />
    </Routes>
  );
}

export default App;
