export interface IUserInfoForm {
  name: {
    value: string;
    touched: boolean;
    valid: boolean;
    error: string;
  };
  username: {
    value: string;
    validation: {
      required: boolean;
      minLength: number;
      maxLength: number;
      startsWithLetter: boolean;
      lettersAndDigitsOnly: boolean;
    };
    touched: boolean;
    valid: boolean;
    error: string;
  };
  email: {
    value: string;
    validation: {
      required: boolean;
      minLength: number;
      maxLength: number;
      isValidEmail: boolean;
    };
    touched: boolean;
    valid: boolean;
    error: string;
  };
  phone: {
    value: string;
    touched: boolean;
    valid: boolean;
    error: string;
  };
  website: {
    value: string;
    touched: boolean;
    valid: boolean;
    error: string;
  };
  addressCity: {
    value: string;
    validation: {
      required: boolean;
      minLength: number;
      maxLength: number;
    };
    touched: boolean;
    valid: boolean;
    error: string;
  };
  addressStreet: {
    value: string;
    validation: {
      required: boolean;
      minLength: number;
      maxLength: number;
    };
    touched: boolean;
    valid: boolean;
    error: string;
  };
  addressSuite: {
    value: string;
    validation: {
      required: boolean;
      minLength: number;
      maxLength: number;
    };
    touched: boolean;
    valid: boolean;
    error: string;
  };
  addressZipcode: {
    value: string;
    touched: boolean;
    valid: boolean;
    error: string;
  };
  companyBS: {
    value: string;
    touched: boolean;
    valid: boolean;
    error: string;
  };
  companyCatchPhrase: {
    value: string;
    touched: boolean;
    valid: boolean;
    error: string;
  };
  companyName: {
    value: string;
    touched: boolean;
    valid: boolean;
    error: string;
  };
}
