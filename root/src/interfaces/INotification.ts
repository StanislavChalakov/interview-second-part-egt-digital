export interface INotification {
  showNotification: boolean;
  notificationType: string;
  notificationMessage: string;
  showToast: boolean;
}
