import { Spin } from 'antd';
import { useAppSelector } from '../../hooks';

export default function Spinner() {
  const { notificationMessage } = useAppSelector((state) => state.ui);

  return (
    <div
      style={{
        marginTop: '45vh',
        marginLeft: '50vw',
        position: 'fixed',
      }}
    >
      <Spin tip={notificationMessage} size="large" />
    </div>
  );
}
