import { Layout, Menu, theme } from 'antd';
import { Link } from 'react-router-dom';
import Spinner from './Spinner';
import { NotificationType } from '../common/notification-type.enum';
import { useAppSelector } from '../../hooks';

const { Header, Content, Footer } = Layout;

type Props = {
  children: React.ReactNode;
};

const AppLayout: React.FC<Props> = ({ children }: Props) => {
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  const { showNotification, notificationType } = useAppSelector(
    (state) => state.ui
  );

  return (
    <Layout className="layout" style={{ minHeight: '100vh' }}>
      <Header style={{ position: 'sticky', top: '0', zIndex: '10' }}>
        <Menu
          theme="dark"
          mode="horizontal"
          defaultSelectedKeys={['users']}
          items={[{ label: <Link to="/">Users</Link>, key: 'users' }]}
        />
      </Header>
      <Content style={{ padding: '0 50px', display: 'flex' }}>
        <div
          className="site-layout-content"
          style={{
            background: colorBgContainer,
            width: '100%',
            padding: '24px',
          }}
        >
          {children}
        </div>
      </Content>
      {showNotification && notificationType === NotificationType.LOADING && (
        <Spinner />
      )}
      <Footer style={{ textAlign: 'center' }}>
        Developed by Stanislav Chalakov, April 2023 <sup>&#174;</sup>
      </Footer>
    </Layout>
  );
};

export default AppLayout;
