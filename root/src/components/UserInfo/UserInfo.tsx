import { Alert, Button, Card, Descriptions, Input } from 'antd';
import { EditOutlined } from '@ant-design/icons';
import { useLocation, useNavigate } from 'react-router-dom';
import {
  Dispatch,
  FormEvent,
  SetStateAction,
  useEffect,
  useState,
} from 'react';
import { userInfoKey } from '../../common/user-info-key.enum';
import { IUserInfoForm } from '../../interfaces/IUserInfoForm';
import {
  containsLettersOnly,
  containsOnlyLettersAndDigits,
  isValidEmail,
} from '../../helpers/helpers';
import { updateUserData } from '../../store/users-actions';
import { useAppDispatch } from '../../../hooks';
import { IUser } from '../../interfaces/IUser';

type Props = {
  user: IUser;
  userIdForEdit: number;
  setUserIdForEdit: Dispatch<SetStateAction<number>>;
  collapseActiveKey: number;
};

export default function UserInfo({
  user,
  userIdForEdit,
  setUserIdForEdit,
  collapseActiveKey,
}: Props) {
  const navigate = useNavigate();
  const { pathname } = useLocation();
  const dispatch = useAppDispatch();
  const [isFormValid, setIsFormValid] = useState(false);
  const [form, setForm] = useState({
    name: {
      value: user.name,
      touched: false,
      valid: true,
      error: '',
    },
    username: {
      value: user.username,
      validation: {
        required: true,
        minLength: 2,
        maxLength: 20,
        startsWithLetter: true,
        lettersAndDigitsOnly: true,
      },
      touched: false,
      valid: true,
      error: '',
    },
    email: {
      value: user.email,
      validation: {
        required: true,
        minLength: 7,
        maxLength: 30,
        isValidEmail: true,
      },
      touched: false,
      valid: true,
      error: '',
    },
    phone: {
      value: user.phone,
      touched: false,
      valid: true,
      error: '',
    },
    website: {
      value: user.website,
      touched: false,
      valid: true,
      error: '',
    },
    addressCity: {
      value: user.address.city,
      validation: {
        required: true,
        minLength: 2,
        maxLength: 30,
      },
      touched: false,
      valid: true,
      error: '',
    },
    addressStreet: {
      value: user.address.street,
      validation: {
        required: true,
        minLength: 5,
        maxLength: 50,
      },
      touched: false,
      valid: true,
      error: '',
    },
    addressSuite: {
      value: user.address.suite,
      validation: {
        required: true,
        minLength: 2,
        maxLength: 30,
      },
      touched: false,
      valid: true,
      error: '',
    },
    addressZipcode: {
      value: user.address.zipcode,
      touched: false,
      valid: true,
      error: '',
    },
    companyBS: {
      value: user.company?.bs,
      touched: false,
      valid: true,
      error: '',
    },
    companyCatchPhrase: {
      value: user.company?.catchPhrase,
      touched: false,
      valid: true,
      error: '',
    },
    companyName: {
      value: user.company?.name,
      touched: false,
      valid: true,
      error: '',
    },
  });

  useEffect(() => {
    if (collapseActiveKey === user.id) {
      resetInitialStateHandler();
    }
  }, [collapseActiveKey]);

  const showPostsHandler = () => {
    navigate(`/users/${user.id}/posts`);
  };

  const inputChangeHandler = (event: FormEvent<HTMLInputElement>) => {
    const { name, value } = event.currentTarget;
    const updateElement = form[name as keyof IUserInfoForm];

    updateElement.touched = true;
    updateElement.valid = isInputValid(value, name);
    updateElement.value = value;

    if (!updateElement.valid) {
      if (name === userInfoKey.USERNAME && !containsLettersOnly(value[0])) {
        updateElement.error = `Username should start with letter`;
      } else if (
        name === userInfoKey.USERNAME &&
        !containsOnlyLettersAndDigits(value)
      ) {
        updateElement.error = `Only letters and numbers allowed`;
      } else if (name === userInfoKey.EMAIL && !isValidEmail(value)) {
        updateElement.error = `Invalid email pattern`;
      } else if ('validation' in updateElement) {
        if (
          'minLength' in updateElement.validation &&
          value.length < updateElement.validation.minLength
        ) {
          updateElement.error = `Minimum lenght: ${updateElement.validation.minLength}`;
        } else if (
          'maxLength' in updateElement.validation &&
          value.length > updateElement.validation.maxLength
        ) {
          updateElement.error = `Maximum lenght: ${updateElement.validation.maxLength}`;
        }
      }
    }

    const updateForm = { ...form, [name]: updateElement };
    setForm(updateForm);

    const areAllFormFieldsValid = Object.values(updateForm).every(
      (element) => element.valid
    );
    setIsFormValid(areAllFormFieldsValid);
  };

  const isInputValid = (value: string, name: string) => {
    const updateElement = form[name as keyof IUserInfoForm];
    let isValid = true;

    if ('validation' in updateElement) {
      if ('startsWithLetter' in updateElement.validation) {
        isValid = isValid && containsLettersOnly(value[0]);
      }

      if ('lettersAndDigitsOnly' in updateElement.validation) {
        isValid = isValid && containsOnlyLettersAndDigits(value);
      }

      if ('isValidEmail' in updateElement.validation) {
        isValid = isValid && isValidEmail(value);
      }

      if ('minLength' in updateElement.validation) {
        isValid = isValid && value.length >= updateElement.validation.minLength;
      }

      if ('maxLength' in updateElement.validation) {
        isValid = isValid && value.length <= updateElement.validation.maxLength;
      }
    }

    return isValid;
  };

  const updateUserHandler = () => {
    dispatch(
      updateUserData({
        id: user.id,
        name: form.name.value,
        email: form.email.value,
        phone: form.phone.value,
        username: form.username.value,
        website: form.website.value,
        address: {
          street: form.addressStreet.value,
          suite: form.addressSuite.value,
          city: form.addressCity.value,
          zipcode: form.addressZipcode.value,
          geo: { ...user.address.geo },
        },
        company: {
          bs: form.companyBS.value,
          catchPhrase: form.companyCatchPhrase.value,
          name: form.companyName.value,
        },
      })
    );

    setUserIdForEdit(0);
    setIsFormValid(false);
  };

  const resetInitialStateHandler = () => {
    setForm((form) => ({
      ...form,
      name: {
        ...form.name,
        value: user.name,
        touched: false,
        valid: true,
      },
      username: {
        ...form.username,
        value: user.username,
        touched: false,
        valid: true,
      },
      email: {
        ...form.email,
        value: user.email,
        touched: false,
        valid: true,
      },
      phone: {
        ...form.phone,
        value: user.phone,
        touched: false,
        valid: true,
      },
      website: {
        ...form.website,
        value: user.website,
        touched: false,
        valid: true,
      },
      addressCity: {
        ...form.addressCity,
        value: user.address.city,
        touched: false,
        valid: true,
      },
      addressStreet: {
        ...form.addressStreet,
        value: user.address.street,
        touched: false,
        valid: true,
      },
      addressSuite: {
        ...form.addressSuite,
        value: user.address.suite,
        touched: false,
        valid: true,
      },
      addressZipcode: {
        ...form.addressZipcode,
        value: user.address.zipcode,
        touched: false,
        valid: true,
      },
      companyBS: {
        ...form.companyBS,
        value: user.company?.bs,
        touched: false,
        valid: true,
      },
      companyCatchPhrase: {
        ...form.companyCatchPhrase,
        value: user.company?.catchPhrase,
        touched: false,
        valid: true,
      },
      companyName: {
        ...form.companyName,
        value: user.company?.name,
        touched: false,
        valid: true,
      },
    }));

    setUserIdForEdit(0);
    setIsFormValid(false);
  };

  return (
    <Card
      title="User Details"
      extra={
        userIdForEdit !== user.id ? (
          <>
            {!pathname.includes('posts') && (
              <Button
                type="primary"
                style={{ marginRight: '0.5rem' }}
                onClick={showPostsHandler}
              >
                See Posts
              </Button>
            )}
            <Button type="primary" onClick={() => setUserIdForEdit(user.id)}>
              <EditOutlined />
            </Button>
          </>
        ) : (
          <>
            {isFormValid && (
              <Button
                type="primary"
                style={{ marginLeft: '0.5rem', marginRight: '0.5rem' }}
                onClick={updateUserHandler}
              >
                Confirm
              </Button>
            )}

            <Button onClick={resetInitialStateHandler}>Cancel</Button>
          </>
        )
      }
    >
      <Descriptions layout="vertical" size="small" bordered>
        <Descriptions.Item label="Name">
          {userIdForEdit !== user.id ? (
            user.name
          ) : (
            <Input
              name={userInfoKey.NAME}
              defaultValue={user.name}
              onChange={inputChangeHandler}
            />
          )}
        </Descriptions.Item>
        <Descriptions.Item label="Username">
          {userIdForEdit !== user.id ? (
            user.username
          ) : (
            <>
              <Input
                name={userInfoKey.USERNAME}
                defaultValue={user.username}
                onChange={inputChangeHandler}
              />
              {form.username.touched && !form.username.valid && (
                <Alert
                  message={form.username.error}
                  type="error"
                  style={{ marginTop: '0.25rem', padding: '0.25rem 0.5rem' }}
                />
              )}
            </>
          )}
        </Descriptions.Item>
        <Descriptions.Item label="Email">
          {userIdForEdit !== user.id ? (
            user.email
          ) : (
            <>
              <Input
                name={userInfoKey.EMAIL}
                defaultValue={user.email}
                onChange={inputChangeHandler}
              />
              {form.email.touched && !form.email.valid && (
                <Alert
                  message={form.email.error}
                  type="error"
                  style={{ marginTop: '0.25rem', padding: '0.25rem 0.5rem' }}
                />
              )}
            </>
          )}
        </Descriptions.Item>
        <Descriptions.Item label="Phone">
          {userIdForEdit !== user.id ? (
            user.phone
          ) : (
            <Input
              name={userInfoKey.PHONE}
              defaultValue={user.phone}
              onChange={inputChangeHandler}
            />
          )}
        </Descriptions.Item>
        <Descriptions.Item label="Website">
          {userIdForEdit !== user.id ? (
            user.website
          ) : (
            <Input
              name={userInfoKey.WEBSITE}
              defaultValue={user.website}
              onChange={inputChangeHandler}
            />
          )}
        </Descriptions.Item>
        <Descriptions.Item label="City">
          {userIdForEdit !== user.id ? (
            user.address?.city
          ) : (
            <>
              <Input
                name={userInfoKey.ADDRESS_CITY}
                defaultValue={user.address?.city}
                onChange={inputChangeHandler}
              />
              {form.addressCity.touched && !form.addressCity.valid && (
                <Alert
                  message={form.addressCity.error}
                  type="error"
                  style={{ marginTop: '0.25rem', padding: '0.25rem 0.5rem' }}
                />
              )}
            </>
          )}
        </Descriptions.Item>
        <Descriptions.Item label="Street">
          {userIdForEdit !== user.id ? (
            user.address?.street
          ) : (
            <>
              <Input
                name={userInfoKey.ADDRESS_STREET}
                defaultValue={user.address?.street}
                onChange={inputChangeHandler}
              />
              {form.addressStreet.touched && !form.addressStreet.valid && (
                <Alert
                  message={form.addressStreet.error}
                  type="error"
                  style={{ marginTop: '0.25rem', padding: '0.25rem 0.5rem' }}
                />
              )}
            </>
          )}
        </Descriptions.Item>
        <Descriptions.Item label="Suite">
          {userIdForEdit !== user.id ? (
            user.address?.suite
          ) : (
            <>
              <Input
                name={userInfoKey.ADDRESS_SUITE}
                defaultValue={user.address?.suite}
                onChange={inputChangeHandler}
              />
              {form.addressSuite.touched && !form.addressSuite.valid && (
                <Alert
                  message={form.addressSuite.error}
                  type="error"
                  style={{ marginTop: '0.25rem', padding: '0.25rem 0.5rem' }}
                />
              )}
            </>
          )}
        </Descriptions.Item>
        <Descriptions.Item label="Zipcode">
          {userIdForEdit !== user.id ? (
            user.address?.zipcode
          ) : (
            <Input
              name={userInfoKey.ADDRESS_ZIPCODE}
              defaultValue={user.address?.zipcode}
              onChange={inputChangeHandler}
            />
          )}
        </Descriptions.Item>
        <Descriptions.Item label="Company BS">
          {userIdForEdit !== user.id ? (
            user.company?.bs
          ) : (
            <Input
              name={userInfoKey.COMPANY_BS}
              defaultValue={user.company?.bs}
              onChange={inputChangeHandler}
            />
          )}
        </Descriptions.Item>
        <Descriptions.Item label="Company catch phrase">
          {userIdForEdit !== user.id ? (
            user.company?.catchPhrase
          ) : (
            <Input
              name={userInfoKey.COMPANY_CATCH_PHRASE}
              defaultValue={user.company?.catchPhrase}
              onChange={inputChangeHandler}
            />
          )}
        </Descriptions.Item>
        <Descriptions.Item label="Company name">
          {userIdForEdit !== user.id ? (
            user.company?.name
          ) : (
            <Input
              name={userInfoKey.COMPANY_NAME}
              defaultValue={user.company?.name}
              onChange={inputChangeHandler}
            />
          )}
        </Descriptions.Item>
      </Descriptions>
    </Card>
  );
}
