import { Button, Card, Input, Popconfirm } from 'antd';
import {
  CheckOutlined,
  CloseOutlined,
  DeleteOutlined,
  EditOutlined,
} from '@ant-design/icons';
import { removePostData, updatePostData } from '../../store/posts-actions';
import { useAppDispatch } from '../../../hooks';
import TextArea from 'antd/es/input/TextArea';
import { Dispatch, SetStateAction, useState } from 'react';
import { IPost } from '../../interfaces/IPost';

type Props = {
  post: IPost;
  postIdForEdit: number;
  setPostIdForEdit: Dispatch<SetStateAction<number>>;
};

export default function UserPost({
  post,
  postIdForEdit,
  setPostIdForEdit,
}: Props) {
  const dispatch = useAppDispatch();
  const [postTitle, setPostTitle] = useState(post.title);
  const [postBody, setPostBody] = useState(post.body);

  const removePostHandler = () => {
    dispatch(removePostData(post.id));
  };

  const updatePostHandler = () => {
    if (postTitle === post.title && postBody === post.body) {
      return setPostIdForEdit(0);
    }

    dispatch(
      updatePostData({
        id: post.id,
        title: postTitle,
        body: postBody,
        userId: post.userId,
      })
    );

    setPostIdForEdit(0);
  };

  const cancelEditHandler = () => {
    setPostTitle(post.title);
    setPostBody(post.body);
    setPostIdForEdit(0);
  };

  const editPostHandler = () => {
    setPostTitle(post.title);
    setPostBody(post.body);
    setPostIdForEdit(post.id);
  };

  return (
    <Card
      type="inner"
      title={
        postIdForEdit !== post.id ? (
          post.title
        ) : (
          <Input
            defaultValue={post.title}
            onChange={(event) => setPostTitle(event.target.value)}
          />
        )
      }
      style={{ marginBottom: '0.5rem', whiteSpace: 'pre-line' }}
      extra={
        postIdForEdit !== post.id ? (
          <>
            <Popconfirm
              title="Delete the post"
              description="Are you sure to delete this post?"
              onConfirm={removePostHandler}
              okText="Confirm"
              cancelText="Cancel"
            >
              <Button
                type="primary"
                danger
                style={{ marginLeft: '0.5rem', marginRight: '0.5rem' }}
                onClick={() => setPostIdForEdit(0)}
              >
                <DeleteOutlined />
              </Button>
            </Popconfirm>
            <Button type="primary" onClick={editPostHandler}>
              <EditOutlined />
            </Button>
          </>
        ) : (
          <>
            <Button
              type="primary"
              style={{ marginLeft: '0.5rem', marginRight: '0.5rem' }}
              onClick={updatePostHandler}
            >
              <CheckOutlined />
            </Button>
            <Button onClick={cancelEditHandler}>
              <CloseOutlined />
            </Button>
          </>
        )
      }
    >
      {postIdForEdit !== post.id ? (
        post.body
      ) : (
        <TextArea
          autoSize={{ minRows: 3, maxRows: 6 }}
          defaultValue={post.body}
          onChange={(event) => setPostBody(event.target.value)}
        />
      )}
    </Card>
  );
}
