import axios from 'axios';
import { AppDispatch } from './store';
import { uiActions } from './ui-slice';
import { NotificationType } from '../common/notification-type.enum';
import { NotificationMessage } from '../common/notification-message.enum';
import { postsActions } from './posts-slice';
import { IPost } from '../interfaces/IPost';

export const fetchUserPostsData = (userId?: string) => {
  return async (dispatch: AppDispatch) => {
    dispatch(
      uiActions.toggleNotification({
        showNotification: true,
        notificationType: NotificationType.LOADING,
        notificationMessage: NotificationMessage.LOADING,
      })
    );

    const fetchUserData = async (userId?: string) => {
      const response = await axios(
        `https://jsonplaceholder.typicode.com/users/${userId}`
      );

      if (response.status !== 200) {
        throw new Error(NotificationMessage.ERROR);
      }

      return response.data;
    };

    const fetchPostsData = async (userId?: string) => {
      const response = await axios(
        `https://jsonplaceholder.typicode.com/posts?userId=${userId}`
      );

      if (response.status !== 200) {
        throw new Error(NotificationMessage.ERROR);
      }

      return response.data;
    };

    try {
      const userData = await fetchUserData(userId);
      const postsData = await fetchPostsData(userId);

      dispatch(postsActions.setUser(userData));
      dispatch(postsActions.setUserPosts(postsData));
      dispatch(
        uiActions.toggleNotification({
          showNotification: false,
          notificationType: '',
          notificationMessage: '',
        })
      );
    } catch (error) {
      dispatch(
        uiActions.toggleNotification({
          showNotification: true,
          notificationType: NotificationType.ERROR,
          notificationMessage: (error as Error).message,
        })
      );
      dispatch(uiActions.toggleToast(true));
    }
  };
};

export const removePostData = (postId: number) => {
  return async (dispatch: AppDispatch) => {
    dispatch(
      uiActions.toggleNotification({
        showNotification: true,
        notificationType: NotificationType.LOADING,
        notificationMessage: NotificationMessage.LOADING,
      })
    );

    const removePost = async (postId: number) => {
      const response = await axios(
        `https://jsonplaceholder.typicode.com/posts/${postId}`,
        {
          method: 'DELETE',
        }
      );

      if (response.status !== 200) {
        throw new Error(NotificationMessage.ERROR);
      }
    };

    try {
      await removePost(postId);

      dispatch(postsActions.removePost(postId));
      dispatch(
        uiActions.toggleNotification({
          showNotification: true,
          notificationType: NotificationType.SUCCESS,
          notificationMessage: NotificationMessage.SUCCESS,
        })
      );
    } catch (error) {
      dispatch(
        uiActions.toggleNotification({
          showNotification: true,
          notificationType: NotificationType.ERROR,
          notificationMessage: (error as Error).message,
        })
      );
    }
    dispatch(uiActions.toggleToast(true));
  };
};

export const updatePostData = (post: IPost) => {
  return async (dispatch: AppDispatch) => {
    dispatch(
      uiActions.toggleNotification({
        showNotification: true,
        notificationType: NotificationType.LOADING,
        notificationMessage: NotificationMessage.LOADING,
      })
    );

    const updatePost = async (post: IPost) => {
      const response = await axios(
        `https://jsonplaceholder.typicode.com/posts/${post.id}`,
        {
          method: 'PUT',
          data: post,
          headers: {
            'Content-type': 'application/json; charset=UTF-8',
          },
        }
      );

      if (response.status !== 200) {
        throw new Error(NotificationMessage.ERROR);
      }

      return response.data;
    };

    try {
      const postData = await updatePost(post);

      dispatch(postsActions.updatePost(postData));
      dispatch(
        uiActions.toggleNotification({
          showNotification: true,
          notificationType: NotificationType.SUCCESS,
          notificationMessage: NotificationMessage.SUCCESS,
        })
      );
    } catch (error) {
      dispatch(
        uiActions.toggleNotification({
          showNotification: true,
          notificationType: NotificationType.ERROR,
          notificationMessage: (error as Error).message,
        })
      );
    }
    dispatch(uiActions.toggleToast(true));
  };
};
