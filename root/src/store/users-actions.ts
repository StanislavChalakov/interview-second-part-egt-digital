import axios from 'axios';
import { AppDispatch } from './store';
import { usersActions } from './users-slice';
import { uiActions } from './ui-slice';
import { NotificationType } from '../common/notification-type.enum';
import { NotificationMessage } from '../common/notification-message.enum';
import { postsActions } from './posts-slice';
import { IUser } from '../interfaces/IUser';

export const fetchUsersData = () => {
  return async (dispatch: AppDispatch) => {
    dispatch(
      uiActions.toggleNotification({
        showNotification: true,
        notificationType: NotificationType.LOADING,
        notificationMessage: NotificationMessage.LOADING,
      })
    );

    const fetchData = async () => {
      const response = await axios(
        'https://jsonplaceholder.typicode.com/users'
      );

      if (response.status !== 200) {
        throw new Error(NotificationMessage.ERROR);
      }

      return response.data;
    };

    try {
      const usersData = await fetchData();

      dispatch(usersActions.setUsers(usersData));
      dispatch(
        uiActions.toggleNotification({
          showNotification: false,
          notificationType: '',
          notificationMessage: '',
        })
      );
    } catch (error) {
      dispatch(
        uiActions.toggleNotification({
          showNotification: true,
          notificationType: NotificationType.ERROR,
          notificationMessage: (error as Error).message,
        })
      );
      dispatch(uiActions.toggleToast(true));
    }
  };
};

export const updateUserData = (user: IUser) => {
  return async (dispatch: AppDispatch) => {
    dispatch(
      uiActions.toggleNotification({
        showNotification: true,
        notificationType: NotificationType.LOADING,
        notificationMessage: NotificationMessage.LOADING,
      })
    );

    const updateUser = async (user: IUser) => {
      const response = await axios(
        `https://jsonplaceholder.typicode.com/users/${user.id}`,
        {
          method: 'PUT',
          data: user,
          headers: {
            'Content-type': 'application/json; charset=UTF-8',
          },
        }
      );

      if (response.status !== 200) {
        throw new Error(NotificationMessage.ERROR);
      }

      return response.data;
    };

    try {
      const userData = await updateUser(user);

      dispatch(usersActions.updateUser(userData));
      dispatch(postsActions.updateUser(userData));
      dispatch(
        uiActions.toggleNotification({
          showNotification: true,
          notificationType: NotificationType.SUCCESS,
          notificationMessage: NotificationMessage.SUCCESS,
        })
      );
    } catch (error) {
      dispatch(
        uiActions.toggleNotification({
          showNotification: true,
          notificationType: NotificationType.ERROR,
          notificationMessage: (error as Error).message,
        })
      );
    }
    dispatch(uiActions.toggleToast(true));
  };
};
