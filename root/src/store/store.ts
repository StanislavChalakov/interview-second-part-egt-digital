import { configureStore } from '@reduxjs/toolkit';
import usersReducer from './users-slice';
import uiReducer from './ui-slice';
import postsReducer from './posts-slice';

const store = configureStore({
  reducer: { users: usersReducer, ui: uiReducer, userPosts: postsReducer },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;
