import { createSlice } from '@reduxjs/toolkit';
import { IUser } from '../interfaces/IUser';

interface IInitialState {
  users: IUser[];
}

const initialState: IInitialState = {
  users: [],
};

const usersSlice = createSlice({
  name: 'users',
  initialState,
  reducers: {
    setUsers(state, action) {
      const usersArr = action.payload.filter(
        (newUser: IUser) => !state.users.find((user) => user.id === newUser.id)
      );
      state.users.push(...usersArr);
    },
    updateUser(state, action) {
      const updatedUser = action.payload;
      const existingUser = state.users.find(
        (user) => user.id === updatedUser.id
      );

      if (existingUser) {
        existingUser.name = updatedUser.name;
        existingUser.email = updatedUser.email;
        existingUser.phone = updatedUser.phone;
        existingUser.username = updatedUser.username;
        existingUser.website = updatedUser.website;

        if (existingUser.address) {
          existingUser.address.street = updatedUser.address?.street;
          existingUser.address.city = updatedUser.address?.city;
          existingUser.address.suite = updatedUser.address?.suite;
          existingUser.address.zipcode = updatedUser.address?.zipcode;
        }

        if (existingUser.company) {
          existingUser.company.bs = updatedUser.company?.bs;
          existingUser.company.catchPhrase = updatedUser.company?.catchPhrase;
          existingUser.company.name = updatedUser.company?.name;
        }
      }
    },
  },
});

export const usersActions = usersSlice.actions;

export default usersSlice.reducer;
