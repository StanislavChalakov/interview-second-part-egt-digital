import { createSlice } from '@reduxjs/toolkit';
import { INotification } from '../interfaces/INotification';

const initialState: INotification = {
  showNotification: false,
  notificationType: '',
  notificationMessage: '',
  showToast: false,
};

const uiSlice = createSlice({
  name: 'ui',
  initialState,
  reducers: {
    toggleNotification(state, action) {
      state.showNotification = action.payload.showNotification;
      state.notificationType = action.payload.notificationType;
      state.notificationMessage = action.payload.notificationMessage;
    },
    toggleToast(state, action) {
      state.showToast = action.payload;
    },
  },
});

export const uiActions = uiSlice.actions;

export default uiSlice.reducer;
