import { createSlice } from '@reduxjs/toolkit';
import { IPost } from '../interfaces/IPost';
import { IUser } from '../interfaces/IUser';

interface IInitialState {
  user?: IUser;
  posts: IPost[];
}

const initialState: IInitialState = {
  posts: [],
};

const postsSlice = createSlice({
  name: 'posts',
  initialState,
  reducers: {
    setUser(state, action) {
      state.user = action.payload;
    },
    setUserPosts(state, action) {
      state.posts = action.payload;
    },
    removePost(state, action) {
      state.posts = state.posts.filter((post) => post.id !== action.payload);
    },
    updatePost(state, action) {
      const updatedPost = action.payload;
      const existingPost = state.posts.find(
        (post) => post.id === updatedPost.id
      );

      if (existingPost) {
        existingPost.title = updatedPost.title;
        existingPost.body = updatedPost.body;
      }
    },
    updateUser(state, action) {
      const updatedUser = action.payload;

      if (state.user) {
        state.user.name = updatedUser.name;
        state.user.email = updatedUser.email;
        state.user.phone = updatedUser.phone;
        state.user.username = updatedUser.username;
        state.user.website = updatedUser.website;

        if (state.user.address) {
          state.user.address.street = updatedUser.address?.street;
          state.user.address.city = updatedUser.address?.city;
          state.user.address.suite = updatedUser.address?.suite;
          state.user.address.zipcode = updatedUser.address?.zipcode;
        }

        if (state.user.company) {
          state.user.company.bs = updatedUser.company?.bs;
          state.user.company.catchPhrase = updatedUser.company?.catchPhrase;
          state.user.company.name = updatedUser.company?.name;
        }
      }
    },
  },
});

export const postsActions = postsSlice.actions;

export default postsSlice.reducer;
